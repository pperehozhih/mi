//
//  mi.m
//  mi
//
//  Created by Paul Perekhozhikh on 09.01.14.
//  Copyright (c) 2014 Paul. All rights reserved.
//

#import "mi.h"
#import "OpenUDID.h"
#import <UIKit/UIKit.h>
#import <AdSupport/AdSupport.h>

#include <net/if.h>
#include <sys/sysctl.h>
#include <net/if_dl.h>
#include <sys/utsname.h>

miError miUpdateNsstring(char *string, size_t *len, NSString* nsstring) {
   size_t need_len = strlen([nsstring UTF8String]);
   if (*len <= need_len) {
      *len = need_len + 1;
      return miNeedMore;
   }
   
   if (string == 0)
      return miInvalidArgument;
   memcpy(string, [nsstring UTF8String], need_len);
   string[need_len] = 0;
   return miOk;
}

miError  miGetIDFA(char *string, size_t *len){
   if (len == 0)
      return miInvalidArgument;
   if( [[ASIdentifierManager class] instancesRespondToSelector:@selector(advertisingIdentifier)] ) {
      NSString* udid = [[[ASIdentifierManager sharedManager] performSelector:@selector(advertisingIdentifier)] performSelector:@selector(UUIDString)];
      return miUpdateNsstring(string, len, udid);
   }
   return miUnsupport;
}

miError  miGetUDID(char *string, size_t *len){
   if (len == 0)
      return miInvalidArgument;
   if( [[UIDevice class] instancesRespondToSelector:@selector(uniqueIdentifier)] ) {
      NSString* udid = [[UIDevice currentDevice] performSelector:@selector(uniqueIdentifier)];
      return miUpdateNsstring(string, len, udid);
   }
   return miUnsupport;
}

miError  miGetIDFV(char *string, size_t *len){
   if (len == 0)
      return miInvalidArgument;
   if( [[UIDevice class] instancesRespondToSelector:@selector(identifierForVendor)] ) {
      NSString* udid = [[[UIDevice currentDevice] performSelector:@selector(identifierForVendor)] performSelector:@selector(UUIDString)];
      return miUpdateNsstring(string, len, udid);
   }
   return miUnsupport;
}

miError  miGetMAC(char *string, size_t *len){
   if (len == 0)
      return miInvalidArgument;
   {
      int   mgmtInfoBase[6];
      char  *msgBuffer = 0;
      size_t length = 0;
      
      // Setup the management Information Base (mib)
      mgmtInfoBase[0] = CTL_NET;        // Request network subsystem
      mgmtInfoBase[1] = AF_ROUTE;       // Routing table info
      mgmtInfoBase[2] = 0;
      mgmtInfoBase[3] = AF_LINK;        // Request link layer information
      mgmtInfoBase[4] = NET_RT_IFLIST;  // Request all configured interfaces
      
      if ((mgmtInfoBase[5] = if_nametoindex("en0")) == 0) {
         NSLog(@"if_nametoindex");
         return miUnsupport;
      }
      if (sysctl(mgmtInfoBase, 6, 0, &length, 0, 0) < 0) {
         NSLog(@"sysctl");
         return miUnsupport;
      }
      if (length <= 0) {
         NSLog(@"length");
         return miUnsupport;
      }
      msgBuffer = malloc(length);
      if (sysctl(mgmtInfoBase, 6, msgBuffer, &length, 0, 0) < 0) {
         NSLog(@"mgmtInfoBase");
         free(msgBuffer);
         return miUnsupport;
      }
      
      {
         // Map msgbuffer to interface message structure
         struct if_msghdr *interfaceMsgStruct = (struct if_msghdr *) msgBuffer;
         
         // Map to link-level socket structure
         struct sockaddr_dl *socketStruct = (struct sockaddr_dl *) (interfaceMsgStruct + 1);
         
         // Copy link layer address data in socket structure to an array
         unsigned char macAddress[6];
         memcpy(&macAddress, socketStruct->sdl_data + socketStruct->sdl_nlen, 6);
         
         // Read from char array into a string object, into traditional Mac address format
         NSString *macAddressString = [NSString stringWithFormat:@"%02X:%02X:%02X:%02X:%02X:%02X",
                                       macAddress[0], macAddress[1], macAddress[2],
                                       macAddress[3], macAddress[4], macAddress[5]];
         // Release the buffer memory
         free(msgBuffer);
         if (macAddress[0] == 2 && macAddress[1] == macAddress[2] == macAddress[3] == macAddress[4] == macAddress[5] == 0)
            return miUnsupport;
         return miUpdateNsstring(string, len, macAddressString);
      }
   }
   return miUnsupport;
}

miError  miGetModel(char *string, size_t *len) {
   if (len == 0)
      return miInvalidArgument;
   {
      struct utsname systemInfo;
      uname(&systemInfo);
      return miUpdateNsstring(string, len, [NSString stringWithUTF8String:systemInfo.machine]);
   }
}

miError  miGetVersion(char *string, size_t *len) {
   if (len == 0)
      return miInvalidArgument;
   {
      return miUpdateNsstring(string, len, [UIDevice currentDevice].systemVersion);
   }
}

miError  miGetOpenUDID(char *string, size_t *len) {
   if (len == 0)
      return miInvalidArgument;
   {
      return miUpdateNsstring(string, len, [OpenUDID value]);
   }
}

miError  miGetString(char *string, size_t *len, miType type) {
   switch (type) {
      case miUDID:
         return miGetUDID(string, len);
      case miOpenUDID:
         return miGetOpenUDID(string, len);
      case miMAC:
         return miGetMAC(string, len);
      case miIDFA:
         return miGetIDFA(string, len);
      case miIDFV:
         return miGetIDFV(string, len);
      case miModel:
         return miGetModel(string, len);
      case miOsVersion:
         return miGetVersion(string, len);
      default:
         return miUnsupport;
         break;
   }
}

miError  miGetInt(int *value, size_t *size, miType type) {
   if (value == 0 || size == 0)
      return miInvalidArgument;
   if (*size < sizeof(int))
      return miInvalidArgument;
   switch (type) {
      case miOsVersion:
         {
            char tmp[64];
            size_t len = sizeof(tmp);
            miError result = miOk;
            if ((result = miGetVersion(tmp, &len)) != miOk)
               return result;
            *value = atoi(tmp);
            *size = sizeof(int);
            return miOk;
         }
      default:
         return miUnsupport;
         break;
   }
}

miError  miGetFloat(float *value, size_t *size, miType type) {
   if (value == 0 || size == 0)
      return miInvalidArgument;
   if (*size < sizeof(int))
      return miInvalidArgument;
   switch (type) {
      case miOsVersion:
      {
         char tmp[64];
         size_t len = sizeof(tmp);
         miError result = miOk;
         if ((result = miGetVersion(tmp, &len)) != miOk)
            return result;
         *value = atoi(tmp);
         *size = sizeof(int);
         return miOk;
      }
      default:
         return miUnsupport;
         break;
   }
}
