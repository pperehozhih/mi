//
//  mi.h
//  Mobile Ids
//
//  Created by Paul Perekhozhikh on 09.01.14.
//  Copyright (c) 2014 Paul. All rights reserved.
//

#ifdef cplusplus
extern "C" {
#endif
   
typedef enum {
   miUDID,
   miMAC,
   miOpenUDID,
   miIDFV,
   miIDFA,
   miIMEI,
   miModel,
   miOsVersion
}miType;
   
typedef enum {
   miOk,
   miNo,
   miInvalidOperation,
   miInvalidEnum,
   miInvalidArgument,
   miUnsupport,
   miNeedMore
}miError;
   
miError  miGetString(char *, size_t *, miType);
   
miError  miGetInt(int *, size_t *, miType);
   
miError  miGetFloat(float *, size_t *, miType);
   
#ifdef cplusplus
}
#endif
