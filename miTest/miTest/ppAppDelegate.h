//
//  ppAppDelegate.h
//  miTest
//
//  Created by Paul Perekhozhikh on 10.01.14.
//  Copyright (c) 2014 Paul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ppAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
