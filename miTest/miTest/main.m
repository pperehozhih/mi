//
//  main.m
//  miTest
//
//  Created by Paul Perekhozhikh on 10.01.14.
//  Copyright (c) 2014 Paul. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ppAppDelegate.h"

int main(int argc, char * argv[])
{
   @autoreleasepool {
       return UIApplicationMain(argc, argv, nil, NSStringFromClass([ppAppDelegate class]));
   }
}
