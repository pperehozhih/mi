//
//  ppViewController.h
//  miTest
//
//  Created by Paul Perekhozhikh on 10.01.14.
//  Copyright (c) 2014 Paul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ppViewController : UIViewController

@property (retain, nonatomic) IBOutlet UIButton *idfa;

@property (retain, nonatomic) IBOutlet UIButton *udid;

@property (retain, nonatomic) IBOutlet UILabel *label;

@property (retain, nonatomic) IBOutlet UIButton *idfv;

@property (retain, nonatomic) IBOutlet UIButton *mac;

@property (retain, nonatomic) IBOutlet UIButton *model;

@property (retain, nonatomic) IBOutlet UIButton *version;

@property (retain, nonatomic) IBOutlet UIButton *openUdid;

@end
