//
//  ppViewController.m
//  miTest
//
//  Created by Paul Perekhozhikh on 10.01.14.
//  Copyright (c) 2014 Paul. All rights reserved.
//

#import "ppViewController.h"

#import <mi/mi.h>

@interface ppViewController ()

@end

@implementation ppViewController

- (IBAction)forAll:(miType) type{
   char buf[512];
   size_t len = 512;
   if (miUnsupport == miGetString(buf, &len, type))
   {
      [[self label] setText:@"Unsupport"];
   } else {
      [[self label] setText:[NSString stringWithUTF8String:buf]];
   }
}

- (IBAction)udidTouch:(id)sender {
   [self forAll:miUDID];
}

- (IBAction)idfaTouch:(id)sender {
   [self forAll:miIDFA];
}

- (IBAction)idfvTouch:(id)sender {
   [self forAll:miIDFV];
}

- (IBAction)macTouch:(id)sender {
   [self forAll:miMAC];
}

- (IBAction)modelTouch:(id)sender {
   [self forAll:miModel];
}

- (IBAction)versionTouch:(id)sender {
   [self forAll:miOsVersion];
}

- (IBAction)openUdidTouch:(id)sender {
   [self forAll:miOpenUDID];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
   [_openUdid release];
   [super dealloc];
}
@end
